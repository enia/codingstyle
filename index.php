<?php
/**
 * All classes should be separated and use namespaces,
 * in order take advantage of a PSR-4 compatible autoloader.
 *
 * @author Michael Larsen
 */
ini_set('display_errors', 1);

/**
 * Connection class
 */
class CurlConnection
{
	protected $hostname;

	/**
	 * Constructor
	 *
	 * @param string $hostname
	 */
	public function __construct(string $hostname) {
		$this->hostname = $hostname;
	}

	/**
	 * Sends GET request
	 */
	public function get(string $resource) : string {
		return $this->send($resource, 'GET');
	}

	/**
	 * Sends POST request
	 */
	public function post(string $resource, array $data = []) : string {
		return $this->send($resource, 'POST', $data);
	}

	/**
	 * Sends POST request
	 */
	public function send(string $resource, string $method = 'GET', array $data = []) : string {
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

		$query = null;
		if($method === 'POST') {
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		} else {
			$query = $data ? '?'.http_build_query($data) : null;
		}

		curl_setopt($ch, CURLOPT_URL, $this->hostname.'/'.$resource.$query);

		$body = curl_exec($ch);
		
		curl_close($ch);

		return $body;
	}	
}

/**
 * The Api Requester
 *
 * An easy access api handler
 */
class Api
{
	static private $instance;
	
	/**
	 * @var CurlConnection
	 */
	protected $conn;

	/**
	 * Return singleton instance
	 */
	static public function getInstance() : self {
		if(!self::$instance) {
			self::$instance = new self(new CurlConnection('http://hoej.dk'));
		}
		return self::$instance;
	}

	/**
	 * Constructor
	 *
	 * Cannot be initialized with "new"
	 */
	private function __construct($conn) {
		$this->conn = $conn;
	}
	
	/**
	 * Magic method
	 *
	 * Allows for easy access to api
	 */
	public function __call($method, $arguments) {
		return json_decode($this->conn->get('/tests/api/'.$method), true);
	}
}

/**
 * The service layer.
 * Normally abstracted to data and task specific classes
 */
class Service
{
	/**
	 * @var array
	 */
	protected $orders;

	/**
	 * Return orders containing a product with $name
	 *
	 * @param string $name
	 * @return array
	 */
	public function getOrdersByProductName(string $name) : array {
		$xml = $this->arrayToXml($this->getOrders());

		foreach($xml->xpath('orders/item[./products/item[./name = "'.$name.'"]]') as $element) {
			$order_ids[$element->id[0].''] = 1;
		}

		return $this->getOrdersById($order_ids);
	}

	/**
	 * Return orders containing a product with parts made of $material
	 *
	 * @param string $material
	 * @return array 
	 */
	public function getOrdersByProductPartsMaterial($material) : array {
		$xml = $this->arrayToXml($this->getOrders());

		foreach($xml->xpath('orders/item[./products/item/parts/item[./materials/item/name = "'.$material.'"]]') as $element) {
			$order_ids[$element->id[0].''] = 1;
		}

		return $this->getOrdersById($order_ids);
	}

	/**
	 * Return orders containing products with no parts
	 *
	 * @return array 
	 */
	public function getOrdersByProductsWithoutParts() : array {
		$xml = $this->arrayToXml($this->getOrders());

		foreach($xml->xpath('orders/item[not(./products/item/parts/item)]') as $element) {
			$order_ids[$element->id[0].''] = 1;
		}

		return $this->getOrdersById($order_ids);
	}

	/**
	 * Return orders by id
	 *
	 * @return array 
	 */
	public function getOrdersById(array $order_ids) : array {
		$collection = [];
		foreach($this->getOrders()['orders'] as $order) {
			if(isset($order_ids[$order['id']])) {
				$collection[] = $order;
			}
		}

		return $collection;
	}	

	/**
	 * Return orders from data
	 *
	 * @return array 
	 */
	public function getOrders() : array {
		if(!$this->orders) {
			$this->orders = $this->getMapper()->orders();
		}
		return $this->orders;
	}
	
	/**
	 * Convert array in to an xml doc for easy filtering
	 *
	 * @param array $data
	 * @return SimpleXMLElement
	 */
	protected function arrayToXml(array $data) : SimpleXMLElement {
		$xml = new SimpleXMLElement('<root/>');

		$this->doArrayToXML($data, $xml);

		return $xml;
	}

	/**
	 * Recursive array conversion
	 *
	 * @param array $array
	 * @param SimpleXMLElement $xml
	 */
	protected function doArrayToXML(array  $array, SimpleXMLElement &$xml) {
		foreach ($array as $key => $value) {
			if(is_array($value)){
				if(is_int($key)){
					$key = "item";
				}
				$label = $xml->addChild($key);
				$this->doArrayToXML($value, $label);
			}
			else {
				$xml->addChild($key, $value);
			}
		}
	}
	
	protected function getMapper() : Api {
		return Api::getInstance();
	}
}

// Controller part, would have been separated into a controller class
$service = new Service();
$result = array(
	'Orders containing product "Logitech mouse"' => $service->getOrdersByProductName('Logitech mouse'),
	'Orders containing products with plastic parts' => $service->getOrdersByProductPartsMaterial('Plastic'),
	'Orders containing products without any parts' => $service->getOrdersByProductsWithoutParts()
);
?>
<?php //ekstra: presentation in phtml ?>
<html>
	<head>
		<style>
			html { font-size: 13px; }
			body { font-family: helvetica; }
			table { border-collapse: collapse; font-size: 1rem; width: 100%; }
			table tr:nth-child(even) { background: #eee; }
			td { border: 1px solid #aaa; padding: 2px 10px; }
			th { text-align: left; padding: 2px 10px; }
		</style>
	</head>
	<body>
	<?php foreach($result as $title => $subresult): ?>
		<div>
			<h2><?php echo $title; ?></h2>
			<table cellspacing="0">
				<colgroup>
					<col width="40%">
					<col width="20%">
					<col>
				</colgroup>
				<thead>
					<tr>
						<th>Id</th>
						<th>Dato</th>
						<th>Total</th>
					</tr>
				</thead>
				<?php foreach($subresult as $order): ?>
					<tr>
						<td><?php echo $order['id']; ?></td>
						<td><?php echo date('d/m-Y \k\l. H:i:s', strtotime($order['order_date']['date'].' '.$order['order_date']['timezone'])); ?></td>
						<td><?php echo $order['currency']; ?> <?php echo number_format($order['total'],2,',','.') ?></td>
					</tr>
				<?php endforeach; ?>
			</table>
		</div>
	<?php endforeach; ?>
	</body>
</html>